#import "FlutterVolumePlugin.h"
@import MediaPlayer;
#import <AVFoundation/AVAudioSession.h>

@implementation FlutterVolumePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"flutter_volume"
            binaryMessenger:[registrar messenger]];
  FlutterVolumePlugin* instance = [[FlutterVolumePlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"getVolume" isEqualToString:call.method]) {
      double volume = [self getVolume];
      result(@(volume));
  } if ([@"setVolume" isEqualToString:call.method]) {
        double volume=[call.arguments doubleValue];
        [self setVolume: volume];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            result(nil);
        });
      } else {
    result(FlutterMethodNotImplemented);
  }
}

-(double)getVolume {
    float volume = [[AVAudioSession sharedInstance] outputVolume];
    return volume;
}

-(void) setVolume: (double)volume {
    MPVolumeView* volumeView = [[MPVolumeView alloc] init];

    UISlider* volumeViewSlider = nil;

    for (UIView *view in [volumeView subviews]){
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]){
            volumeViewSlider = (UISlider*)view;
            break;
        }
    }

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.01 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [volumeViewSlider setValue:volume animated:YES];
        [volumeViewSlider sendActionsForControlEvents:UIControlEventTouchUpInside];
    });


}

@end
