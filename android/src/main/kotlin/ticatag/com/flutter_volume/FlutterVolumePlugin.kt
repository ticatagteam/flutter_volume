package ticatag.com.flutter_volume

import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import android.media.AudioManager
import android.content.Context
import android.app.Activity

class FlutterVolumePlugin(val activity: Activity): MethodCallHandler {
  companion object {

    @JvmStatic
    fun registerWith(registrar: Registrar) {
      if (registrar != null && registrar.activity() != null) {
        val channel = MethodChannel(registrar.messenger(), "flutter_volume")
        channel.setMethodCallHandler(FlutterVolumePlugin(registrar.activity()))
      }
    }
  }

  override fun onMethodCall(call: MethodCall, result: Result) {
    if (call.method == "getPlatformVersion") {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    } else if (call.method == "getVolume") {
      val audioManager: AudioManager = activity.getSystemService(Context.AUDIO_SERVICE) as AudioManager
      val volume=audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
      val maxVolume : Int = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
      val normalizedVolume: Double = volume.toDouble() / maxVolume
      result.success(normalizedVolume)
    } else if (call.method == "setVolume") {
      val newVolume: Double = call.arguments as Double
      val audioManager: AudioManager = activity.getSystemService(Context.AUDIO_SERVICE) as AudioManager
      val maxVolume : Int = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
      val volume : Int = (maxVolume * newVolume).toInt()
      audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, volume, 0)
      result.success(null);
    } else
      result.notImplemented()
    }
}
