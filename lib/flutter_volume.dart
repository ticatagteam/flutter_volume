import 'dart:async';

import 'package:flutter/services.dart';

class FlutterVolume {
  static const MethodChannel _channel = const MethodChannel('flutter_volume');

  static Future<double> get volume async {
    final double volume = await _channel.invokeMethod('getVolume');
    return volume;
  }

  static Future<void> setVolume(double volume) async {
    await _channel.invokeMethod('setVolume', volume);
    return;
  }
}
